#Trying to translate https://github.com/NetSPI/MicroBurst/blob/master/Misc/Invoke-EnumerateAzureBlobs.ps1 into bash
#!/bin/bash

#Check for valid .blob.core.windows.net host names
#When enumerated storage account, check for valid containers via Azure REST API.
#If valid container has publoc files, list them

#Basename to prepend or append with a wordlist.

BASENAME="$1"

function show_usage(){
	printf "Usage: $0 basename [options]\n"
	printf "\n"
	printf "Options:\n"
	printf " -w, wordlist to use\n"
	printf " -o, Output file\n"
return 0
}

if [[ $1 == "-h" ]] || [[ "$1" == "-h" ]];then
	show_usage
	else
	show_usage
	fi

